#!/bin/bash

#
# Scan /etc/easyrsa/ dir to find ca and check if the certficates
# issused will expire in 1, 7, 15, 30 or $EASYRSA_CERT_RENEW and
# if is the case, send a warning email with sendmail.
#

source /usr/local/lib/easyrsa/common_function

# reproduce the easyrsa set_var() function to read the "vars" file
set_var() {
    var=$1
    shift
    value="$*"
    eval "export $var=\"\${$var-$value}\""
}

# send a warning message with sendmail
#   $1 must be the ca name
#   $2 must be the certificate name or "crl" if want to send a
#      a warnig for crl renew
#   $3 must be the the number of days which the certfifcate expire
#   $4 must be the certificate end date
send_warn() {
    local ca_name="${1}"
    local elem_name="${2}"
    local day_expire="${3}"
    local cert_enddate="${4}"

    if which sendmail > /dev/null
    then
        if [ "${elem_name}" == "crl" ]
        then
            echo "Sending warning renew email for ${ca_name}/CRL"
            sendmail -t  << EOF
To: root
Subject: WARNING : CRL for the CA ${ca_name} will expire in ${day_expire} days
Content-Transfer-Encoding: 8bit
Content-Type: text/plain; charset=UTF-8

WARNING !

On host $(hostname -f), CRL for the CA "${ca_name}" will expire in ${day_expire} (CRL date end is "${cert_enddate}").

Think to renew it.

EOF
        else
            echo "Sending warning renew email for ${ca_name}/${cert_name}"
            sendmail -t  << EOF
To: root
Subject: WARNING : Certificate ${elem_name} for the CA ${ca_name} will expire in ${day_expire} days
Content-Transfer-Encoding: 8bit
Content-Type: text/plain; charset=UTF-8

WARNING !

On host $(hostname -f), certificate "${cert_name}" for the CA "${ca_name}" will expire in ${day_expire} (cert date end is "${cert_enddate}").

Think to renew it.

EOF
        fi
    else
        echo "WARN : mail can't be sent bacause sendmail can't be found"
    fi
}

# Checks for a ca in /etc/easyrsa/
for_each_ca() {
    local ca_name="${1}"
    local cert_name=""
    local cert_enddate=""
    local crl_enddate=""

    if [ ! -d "/etc/easyrsa/${ca_name}" -a ! -f "/etc/easyrsa/${ca_name}/vars" ] 
    then
        return 1
    fi
	
    cd "/etc/easyrsa/${ca_name}"

    # reset the EASYRSA_ vars wich can be setted from a previous loop
    unset "${!EASYRSA_@}"
    EASYRSA_CALLER=1
    source vars

    # check certificates

    echo "-- check certifcates"
    cd "${EASYRSA_PKI}/issued/"
    for crt in *.crt
    do
        cert_name="${crt%.crt}"
        cert_enddate=$(openssl x509 -in "${crt}" -noout -enddate | cut -d= -f 2) || return 1
        cert_expire_day=$(nb_day_expire "${cert_enddate}")

        # echo info to log it
        if [ "${cert_expire_day}" -gt 0 -a "${cert_expire_day}" -le ${EASYRSA_CERT_RENEW} ]
        then
            echo "${ca_name}/${cert_name}  expire in ${cert_expire_day} days (cert date end is ${cert_enddate}) : NEED TO RENEW IT"
        else
            echo "${ca_name}/${cert_name}  expire in ${cert_expire_day} days (cert date end is ${cert_enddate}) : NO NEED TO RENEW IT"
        fi

        for warn_day in 1 7 15 30 $EASYRSA_CERT_RENEW
        do
            if [ "${cert_expire_day}" == "${warn_day}" ]
            then
                send_warn "${ca_name}" "${cert_name}" "${cert_expire_day}" "${cert_enddate}"
                break
            fi
        done
    done

    # check CRL

    cd "${EASYRSA_PKI}/"
    echo "-- check CRL"
    if [ -f "./crl.pem" ]
    then

        crl_enddate="$(openssl crl -nextupdate -in ./crl.pem -noout | cut -d= -f2)"
        crl_expire_day=$(nb_day_expire "${crl_enddate}")
        
        if [ "${crl_expire_day}" -le 30 ]
        then
            echo "${ca_name}/CRL expire in ${crl_expire_day} days (CRL date end is ${crl_enddate}) : NEED TO RENEW IT"
        elif [ "${crl_expire_day}" -le 0 ]
        then
            echo "${ca_name}/CRL expire in ${crl_expire_day} days (CRL date end is ${crl_enddate}) : CRL EXPIRED"
        else
            echo "${ca_name}/CRL expire in ${crl_expire_day} days (CRL date end is ${crl_enddate}) : NO NEED TO RENEW IT"
        fi

        for warn_day in 1 7 15 30 174
        do
            if [ "${crl_expire_day}" == "${warn_day}" ]
            then
                send_warn "${ca_name}" "crl" "${crl_expire_day}" "${crl_enddate}"
                break
            fi
        done
    else
        echo "No CRL found."
    fi

}

# iterate on each ca
for ca in "/etc/easyrsa/"* 
do
    for_each_ca "${ca##*/}"
done

#vim: set ts=4 sw=4 expandtab

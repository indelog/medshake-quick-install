Yet an another Easy-RSA 3 ansible role
======================================

Yet another Ansible role to setup and mange Easy-RSA 3 PKI.

Actually it download and install the version 3.0.8 of Easy-RSA directly from the Easy-RSA archive (https://github.com/OpenVPN/easy-rsa/releases/), in the future it probably use the distribution package (the version present on Debian 10 do not permit the use of `--passin` and `--passout` parameters which are needed to set passwords in batch mode).

This role permit to manage several CAs with their own configuration. All parameters for the Easy-RSA 3 `vars` can be set by playbook variables (see `templates/vars.j2`). Its goal is to keep very simple so, it does not manage advanced functionality like sub CA. All the created CAs are in their own directory in `/etc/easyrsa/`.

In each CA you can manage several certificates, called *element* by the role. For each element you can :
* create and renew certificates
* revoke certificates (the role manage the CRL file)
* generate a p12

It also setup a task to send a warning email to `root` with `sendmail` before a certificate or CRL will expire (you can use a tool like [msmtp](https://marlam.de/msmtp/) and the `/etc/aliases` to receive the mails directly in you mailbox). For certificates a warning email is sanded when it expire in the value configured in `EASYRSA_CERT_RENEW` days (by default 30) and when it expire in 30, 15, 7 and 1 days. For the CRL the warning email is sanded when CRL expire in 30, 15, 7 and one days. To setup this warning service it use a *systemd* service (`files/easyrsa-warn-renew.service`) and timer (`files/easyrsa-warn-renew.time`) which will run the (`files/warn-renew.sh`). All this file are installed on the managed node in the `/usr/local/`.

Requirements
------------

* Ansible >= 2.7
* Developed and tested on Debian 10. It should work on other main distribution but not tested.
* The system to send and alert by email before a certificate expire require the use of *systemd* and *sendmail*.
* It install `openssl` on the managed node with the Ansible module `package`.

How to use it ?
---------------

So, we want to create on the managed node a CA with 3 element :

* A certificate for a service (OpenVpn by example), which the private key is not encrypted by a pass.
* 2 certificates for the clients which is protected by a pass.

To do this on our host named `myhost` we use this playbook :

```yml
---
- hosts: myhost
  vars:
    easyrsa_alert_renew_service: true
    easyrsa_ca_list:
    - name: myca1
      pass: "secret"
      conf:
        dn: cn_only
        req_country: US
        req_province: "California"
        req_city: "San Francisco"
        req_org: "Copyleft Certificate Co"
        req_email: "me@example.net"
        req_ou: "My Organizational Unit"
        req_cn: "MyCa1"
        key_size: 4096
        algo: rsa
        ca_expire: 3650
        cert_expire: 1080
        cert_renew: 60
        crl_days: 360
      elements:
        - name: openvpn
          type: server
          cn: openvpn.myhost
        - name: client1
          type: client
          cn: client1.openvpn.myhost
          pass: 'secret1'
          gen_p12: true
        - name: client2
          type: client
          pass: 'secret2'
          cn: client2.openvpn.myhost
          gen_p12: true
          
  roles:
    - ansible-role-easyrsa
```

The `easyrsa_alert_renew_service` permit to enable or disable the "warn to renew by email" service (it take a boolean).

The `easyrsa_ca_list` take a list which describe all the CA to setup. Each CA must be identified by a `name`. For each CA we can have :

* `pass` : which set the passphrase to protect the private key of the CA (this is optional).
* `conf` : which set the parameter for the Easy-RSA 3 `vars` file for this CA.
* `elements` : which is a list of certificates managed by the CA. Like the CAs, each *elements* must be identified by a `name`. For each element we have :
  * `type` : to specify the type of certificate (ex `client` or `server`, this is the type used by `easyrsa sign-req` ).
  * `cn` : to precise the common name of the certificate.
  * `pass` : to specify a passphrase for the certificate private key protection (if empty no pass phrase is used).
  * `gen_p12` : to generate a p12 for the element. The pass used to create the p12 is the one provided in `pass`. If `pass` is not set, no p12 is created.

After the first playbook run, the CA `myca1` is initialised on the managed node and certificates described in `elements` are created (by default you can find them on the managed node in the directory `/etc/easyrsa/myca1/pki/`).

Now we want to revoke the certificate for the client `client1`. We can do it by adding `revoked: true` to its property :

```yml
- name: client1
  type: client
  cn: client1.openvpn.myhost
  pass: 'secret1'
  gen_p12: true
  revoked: true <--- add this if you want the certificate will be revoked on next playbook run
```

When we re run the playbook, the certificate will be revoked and the CRL will be updated.

If we remove later the `revoked: true` property on the element (or set it to false), a new certificate will be issued for this element.

Now, how to renew certificates ? By default when we re-run the playbook, if an issued certificate expire in a number of days less than that provided by the `conf.cert_renew` (if not set the default value is `30`) the certificate will be renewed. To disable this "auto-renew" feature set the parameter `renew: false` on the element. By example if we don't want "auto-renew" `client2` we use this : 

```yml
- name: client2
  type: client
  pass: 'secret2'
  cn: client2.openvpn.myhost
  gen_p12: true
  renew: false <-- add this if you want the certificate new will never be renewed at the new playbook run
```

And what about the CRL refresh ? By default if the CRL expire in less than 30 days at the moment of playbook is run, the CRL will be renewed. The value of 30 days can be modified be using the parameter `crl_renew_day` on the CA properties like this :

```
easyrsa_ca_list:
- name: myca1
  pass: "secret"
  crl_renew_day: 60 <-- now the CRL will be renewed if it expire in less than 30 days at the moment of playbook is run
  conf:
```

If you want create a second CA for another service add an item to the `easyrsa_ca_list` like the first one with another `name` and parameters.


All the role variables
----------------------

This role have 3 level of parameters :

* Global parameters for this role.
* Parameters specific for a CA.
* Parameters specific for an element of the CA.

See above to view how to use them.

### Global parameters

| name                        | definition                                                                         | default value | required |
|-----------------------------|------------------------------------------------------------------------------------|---------------|----------|
| easyrsa_alert_renew_service | true or false. Enable the service to alert by mail when a cert need to be renewed. | false         | no       |
| easyrsa_ca_list             | A list of CA to create and manage.                                                 | empty         | yes      |

# Parameters specific for a CA

All of this parameters describe a CA property and are nested in a item in `easyrsa_ca_list` like this :

```yml
easyrsa_ca_list:
- name: myca1
  pass: 'secret'
  conf: ...
  elements: ...
⁻ name: myca2
  ...
```

| name              | definition                                                                                            | default value                  | required |
|-------------------|-------------------------------------------------------------------------------------------------------|--------------------------------|----------|
| name              | The name of the CA (used to name its directory), use only alphanumeric chars and underscore for this. | none                           | yes      |
| pass              | The passphrase to protect the CA private key.                                                         | empty                          | no       |
| crl_renew_day     | When run playbook, the CRL will renewed if it expire in days value provided by this parameter.        | 30                             | no       |
| gen_dh            | Set it to `false` if you don't want gen a `dh.pem`                                                    | true                           | no       |
| conf              | A list of parameters for Easy-RSA 3 *vars* (see `templates/vars.j2`).                                 | empty                          | yes      |
| conf.dn           | set EASYRSA_DN in Easy-RSA 3 *vars*                                                                   | cn_only                        | no       |
| conf.req_country  | set EASYRSA_REQ_COUNTRY in Easy-RSA 3 *vars*                                                          | US                             | no       |
| conf.req_province | set EASYRSA_REQ_PROVINCE in Easy-RSA 3 *vars*                                                         | California                     | no       |
| conf.req_city     | set EASYRSA_REQ_CITY in Easy-RSA 3 *vars*                                                             | San Francisco                  | no       |
| conf.req_org      | set EASYRSA_REQ_ORG in Easy-RSA 3 *vars*                                                              | Copyleft Certificate Co        | no       |
| conf.req_email    | set EASYRSA_REQ_EMAIL in Easy-RSA 3 *vars*                                                            | me@example.net                 | no       |
| conf.req_ou       | set EASYRSA_REQ_OU in Easy-RSA 3 *vars*                                                               | My Organizational Unit         | no       |
| conf.req_cn       | set EASYRSA_REQ_CN in Easy-RSA 3 *vars*                                                               | ChangeMe                       | no       |
| conf.key_size     | set EASYRSA_KEY_SIZE in Easy-RSA 3 *vars*                                                             | 2048                           | no       |
| conf.algo         | set EASYRSA_ALGO in Easy-RSA 3 *vars*                                                                 | rsa                            | no       |
| conf.curve        | set EASYRSA_CURVE in Easy-RSA 3 *vars*                                                                | secp384r1                      | no       |
| conf.digest       | set EASYRSA_DIGEST in Easy-RSA 3 *vars*                                                               | sha256                         | no       |
| conf.ca_expire    | set EASYRSA_CA_EXPIRE in Easy-RSA 3 *vars*                                                            | 3650                           | no       |
| conf.cert_expire  | set EASYRSA_CERT_EXPIRE in Easy-RSA 3 *vars*                                                          | 1080                           | no       |
| conf.cert_renew   | set EASYRSA_CERT_RENEW in Easy-RSA 3 *vars*                                                           | 30                             | no       |
| conf.crl_days     | set EASYRSA_CRL_DAYS in Easy-RSA 3 *vars*                                                             | 180                            | no       |
| conf.rand_sn      | set EASYRSA_RAND_SN in Easy-RSA 3 *vars*                                                              | yes                            | no       |
| conf.ns_support   | set EASYRSA_NS_SUPPORT in Easy-RSA 3 *vars*                                                           | no                             | no       |
| conf.ns_comment   | set EASYRSA_NS_COMMENT in Easy-RSA 3 *vars*                                                           | Easy-RSA Generated Certificate | no       |
| conf.temp_file    | set EASYRSA_TEMP_FILE in Easy-RSA 3 *vars*                                                            | $EASYRSA_PKI/extensions.temp   | no       |
| conf.ext_dir      | set EASYRSA_EXT_DIR in Easy-RSA 3 *vars*                                                              | $EASYRSA/x509-types            | no       |
| conf.ssl_conf     | set EASYRSA_SSL_CONF in Easy-RSA 3 *vars*                                                             | $EASYRSA/openssl-easyrsa.cnf   | no       |
| elements          | A list of items which describe certificates managed by the ca.                                        | emtpy                          | yes      |

# Parameters specific for an element

This parameters describe an element property for a CA. It must be nested into an `elements` value for a CA like this :

```yml
elements:
- name: client1
  type: client
  pass: 'secret1'
- name: client2
  type: client
  pass: 'secret2'
```

| name    | definition                                                                                                                                                                                                                                 | default value | required |
|---------|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|---------------|----------|
| name    | The name of the element.                                                                                                                                                                                                                   | empty         | yes      |
| type    | The type of certificate to create (ex : `client`/`server`), this correspond to *type* provided in `easyrsa sign-req`.                                                                                                                         | serverClient  | no       |
| cn      | The common name for the certificate. If not provided the `name` of the element will be used.                                                                                                                                                | empty         | no       |
| pass    | Passphrase to protect the certificate private key. If empty the private key of the certificate will no be encrypted.                                                                                                                          | empty         | no       |
| gen_p12 | true or false. If is `true` and `pass` is set, generate a p12 for the element.                                                                                                                                                             | false         | no       |
| revoked | true or false. If is set to `true`, revoke the certificate for the element.                                                                                                                                                                 | false         | no       |
| renew   | true or false. If set to true the certificate will be auto renewed when it expire in a number of days less than that provided by the `conf.cert_renew` at the next playbook run. If set to false the certificate will never by auto renewed. | true          | no       |

Dependencies
------------

This role has no dependency.

Licence
-------

GPLv3

TODO
----

* Renew CA when expire (and warn email)
* Use another user than root to manage ca and run the script that send warning email
* Importing existing CA
* Push on the control node issued certificate and p12
* Gen DH

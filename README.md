# Installation rapide de MedShake EHR/EDC avec Ansible

Utilisation d'un playbook Ansible pour déployer rapidement une installation MedSHake EHR/EDC sur un serveur Debian 11 sur localhost.

Site officiel MedShake EHR/EDC : <https://www.logiciel-cabinet-medical.fr/>
